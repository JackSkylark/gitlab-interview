﻿// See https://aka.ms/new-console-template for more information

var count = GetRequiredConferenceRoomCount(new List<Meeting>
{
    /*new Meeting(
        new (2023, 8, 8, 6, 0, 0),
        new (2023, 8, 8, 7, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 8, 0, 0),
        new (2023, 8, 8, 10, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 7, 0, 0),
        new (2023, 8, 8, 9, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 7, 0, 0),
        new (2023, 8, 8, 9, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 1, 0, 0),
        new (2023, 8, 8, 3, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 2, 0, 0),
        new (2023, 8, 8, 5, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 3, 0, 0),
        new (2023, 8, 8, 4, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 4, 0, 0),
        new (2023, 8, 8, 5, 0, 0)),
    new Meeting(
        new (2023, 8, 8, 5, 0, 0),
        new (2023, 8, 8, 8, 0, 0)),*/
    // new Meeting(
    //     new (2023, 8, 1, 12, 30, 0),
    //     new (2023, 8, 1, 13, 30, 0)),
    // new Meeting(
    //     new (2023, 8, 1, 14, 0, 0),
    //     new (2023, 8, 1, 15, 0, 0))
});

Console.WriteLine(count);

int GetRequiredConferenceRoomCount(
    IReadOnlyList<Meeting> meetings)
{
    if (meetings.Count == 0)
    {
        return 0;
    }
    
    var maxTotalOverlaps = 0;
    for (var i = 0; i < meetings.Count; i++)
    {
        var meetingOverlaps = 0;
        var meeting = meetings[i];

        for (var j = 0; j < meetings.Count; j++)
        {
            var nextMeeting = meetings[j];
            if (meeting == nextMeeting)
            {
                continue;
            }

            if (OverlapsMeeting(meeting, nextMeeting))
            {
                meetingOverlaps++;
            }
        }

        maxTotalOverlaps = Math.Max(maxTotalOverlaps, meetingOverlaps);
    }

    return maxTotalOverlaps + 1;

    bool OverlapsMeeting(
        Meeting a,
        Meeting b)
    {
        return (a.start > b.start && a.start < b.end) || (a.end > b.start && a.end < b.end);
    }
}

record Meeting(
    DateTime start, 
    DateTime end);



