namespace MeetingRoomSolver.Test;

public class MeetingTimeUtilitiesTests
{
    [Fact]
    public void GetRequiredMeetingRoomCount_WhenNoMeetings_Returns0()
    {
        var meetingTimes =
            Array.Empty<MeetingTime>();

        var requiredMeetingRooms =
            MeetingTimeUtilities.GetRequiredMeetingRoomCount(
                meetingTimes);

        Assert.Equal(0, requiredMeetingRooms);
    }

    [Fact]
    public void GetRequiredMeetingRoomCount_WhenMeetings_ReturnsRequiredMeetingRoomCount()
    {
        const int actualRequiredMeetingRooms = 3;
        
        var meetingTimes =
            new MeetingTime[]
            {
                new (6, 7),
                new (8, 10),
                new (7, 9),
                new (7, 9),
                new (1, 3),
                new (2, 5),
                new (3, 4),
                new (4, 5),
                new (5, 8)
            };
        
        var requiredMeetingRooms =
            MeetingTimeUtilities.GetRequiredMeetingRoomCount(
                meetingTimes);

        Assert.Equal(actualRequiredMeetingRooms, requiredMeetingRooms);
    }
    
    [Fact]
    public void GetRequiredMeetingRoomCountFast_WhenNoMeetings_Returns0()
    {
        var meetingTimes =
            Array.Empty<MeetingTime>();

        var requiredMeetingRooms =
            MeetingTimeUtilities.GetRequiredMeetingRoomCountFast(
                meetingTimes);

        Assert.Equal(0, requiredMeetingRooms);
    }
    
    [Fact]
    public void GetRequiredMeetingRoomCountFast_WhenMeetings_ReturnsRequiredMeetingRoomCount()
    {
        const int actualRequiredMeetingRooms = 3;
        
        var meetingTimes =
            new MeetingTime[]
            {
                new (6, 7),
                new (8, 10),
                new (7, 9),
                new (7, 9),
                new (1, 3),
                new (2, 5),
                new (3, 4),
                new (4, 5),
                new (5, 8)
            };
        
        var requiredMeetingRooms =
            MeetingTimeUtilities.GetRequiredMeetingRoomCountFast(
                meetingTimes);

        Assert.Equal(actualRequiredMeetingRooms, requiredMeetingRooms);
    }
}