﻿namespace MeetingRoomSolver;

public static class MeetingTimeUtilities
{
    public static int GetRequiredMeetingRoomCount(
        MeetingTime[] meetingTimes)
    {
        if (meetingTimes.Length == 0)
        {
            return 0;
        }

        var sortedMeetingTimes = 
            meetingTimes
                .OrderBy(x => x.StartTime)
                .ToList();

        var totalRequiredMeetingRooms = 0;
        
        var currentRequiredMeetingRooms = 0;
        var windowStartIndex = 0;

        for (var i = 0; i < sortedMeetingTimes.Count; i++)
        {
            var currentMeetingTime = sortedMeetingTimes[i];

            for (var j = windowStartIndex; j < i; j++)
            {
                var jMeetingTime = sortedMeetingTimes[j];
                
                if (IsOverlap(currentMeetingTime, jMeetingTime))
                    continue;

                windowStartIndex++;
                currentRequiredMeetingRooms--;
            }

            currentRequiredMeetingRooms++;
            totalRequiredMeetingRooms = Math.Max(currentRequiredMeetingRooms, totalRequiredMeetingRooms);
        }

        return totalRequiredMeetingRooms + 1;

        bool IsOverlap(
            MeetingTime a,
            MeetingTime b)
        {
            return (a.StartTime > b.StartTime && a.StartTime < b.EndTime) || 
                   (a.EndTime > b.StartTime && a.EndTime < b.StartTime);
        }
    }
    
    public static int GetRequiredMeetingRoomCountFast(
        MeetingTime[] meetingTimes)
    {
        if (meetingTimes.Length == 0)
        {
            return 0;
        }
        
        var sortedMeetingTimes = 
            meetingTimes
                .OrderBy(x => x.StartTime)
                .ToList();

        var meetingRoomCountByEndTime = new SortedDictionary<int, int>();
        foreach (var meetingTime in sortedMeetingTimes)
        {
            if (meetingRoomCountByEndTime.Count > 0)
            {
                var earliestMeetingRoomEndTime =
                    meetingRoomCountByEndTime.Keys.First();
                
                if (meetingTime.StartTime >= earliestMeetingRoomEndTime)
                {
                    if (--meetingRoomCountByEndTime[earliestMeetingRoomEndTime] == 0)
                    {
                        meetingRoomCountByEndTime.Remove(earliestMeetingRoomEndTime);
                    }
                }
            }

            if (meetingRoomCountByEndTime.ContainsKey(meetingTime.EndTime))
                meetingRoomCountByEndTime[meetingTime.EndTime]++;
            else
                meetingRoomCountByEndTime[meetingTime.EndTime] = 1;
        }

        return meetingRoomCountByEndTime.Sum(x => x.Value);
    }
}